package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var arrayList: ArrayList<Restaurants>
    lateinit var restaurantsImageId: Array<Int>
    lateinit var restaurantsName: Array<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        val controller = findNavController(R.id.nav_host_fragment)
        val appBarConfig = AppBarConfiguration(
            setOf(
                R.id.homeFragment,
                R.id.searchFragment,
                R.id.profileFragment,
            )
        )

        setupActionBarWithNavController(controller, appBarConfig)
        navView.setupWithNavController(controller)

        restaurantsImageId = arrayOf(
            R.drawable.bakeryracha,
            R.drawable.bigsams,
            R.drawable.burgerlions,
            R.drawable.churrosita,
            R.drawable.domino_s_pizza,
            R.drawable.dunkin_,
            R.drawable.firewok,
            R.drawable.gldanis_shaurma,
            R.drawable.ironburger,
            R.drawable.kyoto,
            R.drawable.mc,
            R.drawable.oishisushi,
            R.drawable.parada_s_pica,
            R.drawable.pizza_mafia,
            R.drawable.shemoikhede,
            R.drawable.subway,
            R.drawable.sushi24,
            R.drawable.tiktokcafe,
            R.drawable.ukve,
            R.drawable.wendy_s
        )
        restaurantsName = arrayOf(
            "Bakery Racha",
            "Big Sam's",
            "Burger Lions",
            "Currosita",
            "Domino's pizza",
            "Dunkin'",
            "Fire Wok",
            "Gldanis Shaurma",
            "Iron Burger",
            "Kyoto",
            "McDonald's",
            "Oishi Sushi",
            "Parada's pizza",
            "Pizza mafia",
            "Shemoikhede Genathsvale",
            "Subway",
            "Sushi24",
            "Tik Tok Cafe",
            "Ukve",
            "Wendy's"
        )

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        arrayList = arrayListOf<Restaurants>()
        getUserData()

    }

    private fun getUserData() {
        for (i in restaurantsImageId.indices) {
            val restaurant = Restaurants(restaurantsImageId[i], restaurantsName[i])
            arrayList.add(restaurant)
        }
        recyclerView.adapter = MyAdapter(arrayList)
    }
}
